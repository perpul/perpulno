# perpulno

Node.js client library for Perpul using JSON-RPC

##Node version
Works better with NodeJS v11.16 and further

## Installation

```bash
$ npm install perpulno
```

## Usage

```js
var Perpul = require('perpulno');

var perpul = new Perpul({
  host: 'localhost',
  port: 7073,
  database: 'perpul',
  username: 'admin',
  password: 'perpul'
});

// Connect to Perpul
perpul.connect(function (err) {
  if (err) { return console.log(err); }

  // Get a partner
  perpul.get('res.partner', 4, function (err, partner) {
    if (err) { return console.log(err); }

    console.log('Partner', partner);
  });
});
```

## Methods

### perpul.connect(callback)
### perpul.create(model, params, callback)
### perpul.get(model, id, callback)
### perpul.update(model, id, params, callback)
### perpul.delete(model, id, callback)
### perpul.search(model, params, callback)

## Reference

* [Perpul Technical Documentation](https://www.perpul.co/documentation0)
* [Perpul Web Service API](https://www.perpul.co/documentation/api_integration.html)

## License

The MIT License (MIT)

Copyright (c) 2018 Daniel Podvesker, perpul and all the related trademarks

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
