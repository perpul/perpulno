var assert = require('assert'),
    sinon  = require('sinon'),
    Perpul   = require('../lib/index');

var config = {
  host: 'perpulperpul.vagrantshare.com',
  port: 80,
  database: 'perpul',
  username: 'admin',
  password: 'perpul'
};

var perpul = new Perpul(config);

describe('Perpul', function () {
  this.timeout(3000);

  it('Perpul should be a function', function () {
    assert.equal(typeof Perpul, 'function');
  });

  it('perpul should be an instance of Perpul', function () {
    assert(perpul instanceof Perpul);
  });

  it('perpul should have this properties', function () {
    assert.notEqual(perpul.host, undefined);
    assert.equal(perpul.host, config.host);
    assert.notEqual(perpul.port, undefined);
    assert.equal(perpul.port, config.port);
    assert.notEqual(perpul.database, undefined);
    assert.equal(perpul.database, config.database);
    assert.notEqual(perpul.username, undefined);
    assert.equal(perpul.username, config.username);
    assert.notEqual(perpul.password, undefined);
    assert.equal(perpul.password, config.password);
  });

  it('perpul should have this public functions', function () {
    assert.equal(typeof perpul.connect, 'function');
    assert.equal(typeof perpul.create, 'function');
    assert.equal(typeof perpul.get, 'function');
    assert.equal(typeof perpul.update, 'function');
    assert.equal(typeof perpul.delete, 'function');
    assert.equal(typeof perpul.search, 'function');
  });

  it('perpul should have this private functions', function () {
    assert.equal(typeof perpul._request, 'function');
  });

  describe('Creating client', function () {

    it('client should not be able to connect to perpul server', function (done) {
      var client = new Perpul({
            host: config.host,
            database: 'DatabaseNotFound',
            username: config.username,
            password: config.password
          }),
          callback = sinon.spy();

        client.connect(callback);

        setTimeout(function () {
          assert(callback.called);
          assert.equal(typeof callback.args[0][0], 'object');
          assert.equal(callback.args[0][1], null);

          done();
        }, 2000);
    });

    it('client should be able to connect to perpul server', function (done) {
      var callback = sinon.spy();

      perpul.connect(callback);

      setTimeout(function () {
        assert(callback.calledWith(null));
        assert.equal(typeof callback.args[0][1], 'object');
        assert(perpul.uid);
        assert(perpul.sid);
        assert(perpul.session_id);
        assert(perpul.context);

        done();
      }, 2000);
    });

  });

  describe('Records', function () {

    var created;

    it('client should create a record', function (done) {
      var callback = sinon.spy();
      perpul.create('hr.employee', {
        name: 'John Doe',
        work_email: 'john@doe.com'
      }, callback);

      setTimeout(function () {
        assert(callback.calledWith(null));
        assert.equal(typeof callback.args[0][1], 'number');

        created = callback.args[0][1];

        done();
      }, 2000);

    });

    it('client should get a record', function (done) {
      var callback = sinon.spy();
      perpul.get('hr.employee', created, callback);

      setTimeout(function () {
        assert(callback.calledWith(null));
        assert.equal(typeof callback.args[0][1], 'object');
        assert.equal(callback.args[0][1].display_name, 'John Doe');
        assert.equal(callback.args[0][1].work_email, 'john@doe.com');

        done();
      }, 2000);

    });

    it('client should update a record', function (done) {
      var callback = sinon.spy();
      perpul.update('hr.employee', created, {
        name: 'Jane Doe',
        work_email: 'jane@doe.com'
      }, callback);

      setTimeout(function () {
        assert(callback.calledWith(null));
        assert(callback.args[0][1]);

        done();
      }, 2000);
    });

    it('client should delete a record', function (done) {
      var callback = sinon.spy();
      perpul.delete('hr.employee', created, callback);

      setTimeout(function () {
        assert(callback.calledWith(null));
        assert(callback.args[0][1]);

        done();
      }, 2000);
    });

    it('client should search records', function (done) {
      var callback = sinon.spy();
      perpul.search('hr.employee', [['login', '=', 'admin']], callback);

      setTimeout(function () {
        assert(callback.calledWith(null));
        assert.equal(typeof callback.args[0][1], 'array');

        done();
      }, 2000);
    });

  });

});
