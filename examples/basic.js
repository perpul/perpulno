var Perpul = require('../lib/index');

var perpul = new Perpul({
  host: 'localhost',
  port: 7073,
  database: 'perpul',
  username: 'admin',
  password: 'perpul'
});

// Connect to Perpul
perpul.connect(function (err) {
  if (err) { return console.log(err); }

  // Get a partner
  perpul.get('res.partner', 4, function (err, partner) {
    if (err) { return console.log(err); }

    console.log('Partner', partner);
  });
});
